from functools import *
from math import *
from solid import *
from solid.utils import *

# http://www.openscad.org/cheatsheet/index.html

hauteur_murs = 3000

# Description chambre
def chambre(hauteur_murs = hauteur_murs,
    largeur_chambre = 2500,
    longueur_chambre = 4000,
    epaisseur_mur = 50,
    epaisseur_sol = 50,
    largeur_porte = 1000,
    # Description des couleurs
    couleur_plafond = color([0.9, 0.9, 0.9, 0.2]),
    couleur_mur = color([0.9, 0.9, 0.9, 0.5]),
    couleur_sol = color([0.5, 0.4, 0.3, 0.5])):
    # Definition des murs
    mur_ne = translate([-largeur_chambre,0,0])(cube([largeur_chambre, epaisseur_mur, hauteur_murs]))
    mur_so = translate([0,-longueur_chambre,0])(cube([epaisseur_mur, longueur_chambre, hauteur_murs]))
    sol = translate([-largeur_chambre-largeur_porte,-longueur_chambre-largeur_porte,-epaisseur_sol])(cube([largeur_chambre+largeur_porte+epaisseur_mur, longueur_chambre+largeur_porte+epaisseur_mur, epaisseur_sol]))
    plafond = translate([0,0,epaisseur_sol+hauteur_murs])(sol)
    return couleur_mur(mur_ne + mur_so) + couleur_sol(sol) + couleur_plafond(plafond)

# Description des couleurs
couleur_lit = color([0.8, 0.7, 0.6, 1])
blanc = color([1, 1, 1, 1])
rouge = color([0.8, 0.1, 0.1, 1])
bleu = color([0.1, 0.3, 0.9, 1])
jaune = color([0.9, 0.8, 0.3, 1])

# Dimensions lit


# Description lit
def lit(hauteur_etage = 1700,
    longueur_etage = 2400,
    largeur_etage = 1000,
    hauteur_barriere = 900,
    hauteur_lit = 150,
    epaisseur_lit = 30,
    hauteur_murs = hauteur_murs,
    nombre_marche = 6,
    largeur_marche = 700,
    profondeur_marche = 300,
    epaisseur_planche_marche = 20,
    section_pillier = 60,
    largeur_montant = 40,
    epaisseur_montant = 20,
    hauteur_porte = 1500):
    # Le sommier
    lit_etage = translate([-longueur_etage, -largeur_etage, hauteur_etage-epaisseur_lit])(
        cube([longueur_etage, largeur_etage, epaisseur_lit]))
    lit_bas = translate([-longueur_etage, -largeur_etage, hauteur_lit-epaisseur_lit])(
        cube([longueur_etage, largeur_etage, epaisseur_lit]))
    def pilliers():
        hauteur_marche = (hauteur_etage-epaisseur_planche_marche*(nombre_marche+1))/nombre_marche
        pillier_1 = translate([-longueur_etage, -largeur_etage, 0])(
            cube([section_pillier, section_pillier, hauteur_barriere + hauteur_etage]))
        pillier_2 = translate([-longueur_etage, -section_pillier, 0])(
            cube([section_pillier, section_pillier, hauteur_murs]))
        pillier_3 = translate([-largeur_marche, -largeur_etage, 0])(
            cube([section_pillier, section_pillier, hauteur_barriere + hauteur_etage]))
        pillier_4 = translate([-largeur_marche, -(section_pillier+largeur_etage+epaisseur_planche_marche+(nombre_marche-1)*(profondeur_marche+epaisseur_planche_marche)), 0])(
            cube([section_pillier, section_pillier, hauteur_marche+epaisseur_planche_marche+hauteur_barriere]))
        return pillier_1 + pillier_2 + pillier_3 + pillier_4
    # La barriere
    def barriere():
        result = []
        largeur_espace_cible = 60
        # Cote
        largeur_cote = largeur_etage - 2*section_pillier
        hauteur_cote_haut = hauteur_murs
        hauteur_cote_bas = hauteur_barriere + hauteur_etage
        nombre_montant_cote = int(ceil((largeur_cote-largeur_espace_cible)/float(largeur_espace_cible+largeur_montant)))
        largeur_espace_cote = (largeur_cote-largeur_espace_cible)/float(nombre_montant_cote)-largeur_montant
        for i in range(nombre_montant_cote):
            result.append(translate([-(longueur_etage+epaisseur_montant), -(section_pillier+largeur_montant+largeur_espace_cote+i*(largeur_montant+largeur_espace_cote)), 0])(
                cube([epaisseur_montant, largeur_montant, hauteur_cote_haut + (1+i)*(hauteur_cote_bas-hauteur_cote_haut)/(1+nombre_montant_cote)])))
        # Face
        largeur_face = longueur_etage - section_pillier - largeur_marche
        hauteur_face = hauteur_barriere + hauteur_etage
        nombre_montant_face = int(ceil((largeur_face-largeur_espace_cible)/float(largeur_espace_cible+largeur_montant)))
        largeur_espace_face = (largeur_face-largeur_espace_cible)/float(nombre_montant_face)-largeur_montant
        for i in range(nombre_montant_face):
            result.append(translate([-longueur_etage+section_pillier+largeur_espace_face+i*(largeur_montant+largeur_espace_face), -largeur_etage-epaisseur_montant, 0])(
                cube([largeur_montant, epaisseur_montant, hauteur_face])))
        porte = translate([-longueur_etage+section_pillier+700, -largeur_etage-1.5*epaisseur_montant, 0])(
            cube([700, 2*epaisseur_montant, hauteur_porte]))
        fenetre_1 = translate([-longueur_etage-1.5*epaisseur_montant, -800, hauteur_porte-600])(
            cube([2*epaisseur_montant, 600, 600]))
        fenetre_2 = translate([-longueur_etage+section_pillier+200, -largeur_etage-1.5*epaisseur_montant, hauteur_etage+0.5*hauteur_barriere])(
            cube([700, 2*epaisseur_montant, 200]))
        return reduce(lambda r,x: r+x, result[1:], result[0])-porte-fenetre_1-fenetre_2

    # Description escalier
    def escalier(epaisseur_porte = 10,
        # Description des couleurs
        couleur_lit = couleur_lit,
        couleur_porte_1 = blanc,
        couleur_porte_2 = rouge,
        couleur_porte_3 = jaune):
        hauteur_marche = (hauteur_etage-epaisseur_planche_marche*(nombre_marche+1))/nombre_marche
        # Paroie verticale
        def v(i,j):
            return translate([-largeur_marche,
                -(largeur_etage+epaisseur_planche_marche)-i*(profondeur_marche+epaisseur_planche_marche),
                j*(hauteur_marche+epaisseur_planche_marche)])(
                cube([largeur_marche, epaisseur_planche_marche, 2*epaisseur_planche_marche+hauteur_marche]))
        # Paroie horizontale
        def h(i,j):
            return translate([-largeur_marche,
                -(largeur_etage+profondeur_marche+2*epaisseur_planche_marche)-i*(profondeur_marche+epaisseur_planche_marche),
                j*(hauteur_marche+epaisseur_planche_marche)])(
                cube([largeur_marche, 2*epaisseur_planche_marche+profondeur_marche, epaisseur_planche_marche]))
        # Porte
        def p(i,j,l,h):
            return translate([-(largeur_marche+epaisseur_porte),
                -(largeur_etage+profondeur_marche+epaisseur_planche_marche)-(i+l-1)*(profondeur_marche+epaisseur_planche_marche),
                epaisseur_planche_marche+j*(hauteur_marche+epaisseur_planche_marche)])(
                cube([epaisseur_porte, profondeur_marche+(l-1)*(profondeur_marche+epaisseur_planche_marche), hauteur_marche+(h-1)*(hauteur_marche+epaisseur_planche_marche)]))
        H =[[1,0,0,0,0,0],
            [1,1,0,0,0,0],
            [0,0,1,0,0,0],
            [0,0,1,1,0,0],
            [1,1,0,0,1,0],
            [1,1,1,1,1,0]]
        V =[[1,0,0,0,0,0],
            [1,1,0,0,0,0],
            [1,0,1,0,0,0],
            [1,0,1,1,0,0],
            [1,0,1,0,1,0],
            [1,0,1,0,1,1]]
        result = []
        for i in range(nombre_marche):
            for j in range(nombre_marche):
                if H[j][i]==1:
                    result.append(h(i,nombre_marche-j-1))
                if V[j][i]==1:
                    result.append(v(i,nombre_marche-j-1))
        return couleur_lit(reduce(lambda r,x: r+x, result[1:], result[0]))\
        + couleur_porte_1(p(0,1,2,3))\
        + couleur_porte_2(p(2,0,2,2))\
        + couleur_porte_3(p(2,2,1,1))
    # Construction
    barriere()
    return escalier() + couleur_lit(lit_etage+lit_bas+pilliers()+barriere())

lit_tancrede = chambre() + lit()
#lit_tancrede = lit()

if __name__ == '__main__':
    scad_render_to_file(lit_tancrede,
        'lit_tancrede.scad',
        file_header='$fn={fn};'.format(fn=100))
