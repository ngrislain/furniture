$fn=100;

union() {
	color(c = [0.9000000000, 0.9000000000, 0.9000000000, 0.5000000000]) {
		union() {
			translate(v = [-2500, 0, 0]) {
				cube(size = [2500, 50, 3000]);
			}
			translate(v = [0, -4000, 0]) {
				cube(size = [50, 4000, 3000]);
			}
		}
	}
	color(c = [0.5000000000, 0.4000000000, 0.3000000000, 0.5000000000]) {
		translate(v = [-3500, -5000, -50]) {
			cube(size = [3550, 5050, 50]);
		}
	}
	union() {
		color(c = [0.8000000000, 0.7000000000, 0.6000000000, 1]) {
			union() {
				translate(v = [-700, -1340, 1400.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 1400.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1340, 1120.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 1120.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1020, 840.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1020, 560.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1340, 280.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 280.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1340, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1660, 1120.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1340, 1120.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1660, 280.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1980, 840.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 840.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1980, 560.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 560.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1660, 280.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1980, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2300, 560.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1980, 560.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2300, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -2620, 280.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -2300, 280.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2620, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -2300, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2620, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
			}
			union() {
				translate(v = [-2400, -1000, 1670]) {
					cube(size = [2400, 1000, 30]);
				}
				translate(v = [-2400, -1000, 120]) {
					cube(size = [2400, 1000, 30]);
				}
				union() {
					translate(v = [-2400, -1000, 0]) {
						cube(size = [60, 60, 2600]);
					}
					translate(v = [-2400, -60, 0]) {
						cube(size = [60, 60, 3000]);
					}
					translate(v = [-700, -1000, 0]) {
						cube(size = [60, 60, 2600]);
					}
					translate(v = [-700, -2680, 0]) {
						cube(size = [60, 60, 1200.0000000000]);
					}
				}
				difference() {
					union() {
						translate(v = [-2420, -151.1111111111, 0]) {
							cube(size = [20, 40, 2960.0000000000]);
						}
						translate(v = [-2420, -242.2222222222, 0]) {
							cube(size = [20, 40, 2920.0000000000]);
						}
						translate(v = [-2420, -333.3333333333, 0]) {
							cube(size = [20, 40, 2880.0000000000]);
						}
						translate(v = [-2420, -424.4444444444, 0]) {
							cube(size = [20, 40, 2840.0000000000]);
						}
						translate(v = [-2420, -515.5555555556, 0]) {
							cube(size = [20, 40, 2800.0000000000]);
						}
						translate(v = [-2420, -606.6666666667, 0]) {
							cube(size = [20, 40, 2760.0000000000]);
						}
						translate(v = [-2420, -697.7777777778, 0]) {
							cube(size = [20, 40, 2720.0000000000]);
						}
						translate(v = [-2420, -788.8888888889, 0]) {
							cube(size = [20, 40, 2680.0000000000]);
						}
						translate(v = [-2420, -880.0000000000, 0]) {
							cube(size = [20, 40, 2640.0000000000]);
						}
						translate(v = [-2281.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-2182.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-2083.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1985.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1886.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1787.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1688.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1590.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1491.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1392.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1293.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1195.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1096.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-997.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-898.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-800.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
					}
					translate(v = [-1640, -1030.0000000000, 0]) {
						cube(size = [700, 40, 1500]);
					}
					translate(v = [-2430.0000000000, -800, 900]) {
						cube(size = [40, 600, 600]);
					}
					translate(v = [-2140, -1030.0000000000, 2150.0000000000]) {
						cube(size = [700, 40, 200]);
					}
				}
				union() {
					translate(v = [-680.0000000000, -2620, 930.0000000000]) {
						rotate(a = [41.2568806153, 0, 0]) {
							cube(size = [20, 2214.2516101886, 40]);
						}
					}
					translate(v = [0, 0, -132.9784944561]) {
						translate(v = [-680.0000000000, -2620, 930.0000000000]) {
							rotate(a = [41.2568806153, 0, 0]) {
								cube(size = [20, 2214.2516101886, 40]);
							}
						}
					}
					translate(v = [0, 0, -265.9569889122]) {
						translate(v = [-680.0000000000, -2620, 930.0000000000]) {
							rotate(a = [41.2568806153, 0, 0]) {
								cube(size = [20, 2214.2516101886, 40]);
							}
						}
					}
					translate(v = [0, 0, -398.9354833683]) {
						translate(v = [-680.0000000000, -2620, 930.0000000000]) {
							rotate(a = [41.2568806153, 0, 0]) {
								cube(size = [20, 2214.2516101886, 40]);
							}
						}
					}
				}
			}
		}
		color(c = [1, 1, 1, 1]) {
			translate(v = [-710, -1640, 300.0000000000]) {
				cube(size = [10, 620, 820.0000000000]);
			}
		}
		color(c = [0.8000000000, 0.1000000000, 0.1000000000, 1]) {
			translate(v = [-710, -2280, 20.0000000000]) {
				cube(size = [10, 620, 540.0000000000]);
			}
		}
		color(c = [0.9000000000, 0.8000000000, 0.3000000000, 1]) {
			translate(v = [-710, -1320, 1140.0000000000]) {
				cube(size = [10, 300, 260.0000000000]);
			}
		}
		color(c = [0.8000000000, 0.7000000000, 0.6000000000, 1]) {
			union() {
				translate(v = [-700, -1340, 1400.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 1400.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1340, 1120.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 1120.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1020, 840.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1020, 560.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1340, 280.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 280.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1340, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1020, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1660, 1120.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1340, 1120.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1660, 280.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1980, 840.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 840.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1980, 560.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 560.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1660, 280.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -1980, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1660, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2300, 560.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -1980, 560.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2300, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -2620, 280.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -2300, 280.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2620, 0.0000000000]) {
					cube(size = [700, 340, 20]);
				}
				translate(v = [-700, -2300, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
				translate(v = [-700, -2620, 0.0000000000]) {
					cube(size = [700, 20, 300.0000000000]);
				}
			}
			union() {
				translate(v = [-2400, -1000, 1670]) {
					cube(size = [2400, 1000, 30]);
				}
				translate(v = [-2400, -1000, 120]) {
					cube(size = [2400, 1000, 30]);
				}
				union() {
					translate(v = [-2400, -1000, 0]) {
						cube(size = [60, 60, 2600]);
					}
					translate(v = [-2400, -60, 0]) {
						cube(size = [60, 60, 3000]);
					}
					translate(v = [-700, -1000, 0]) {
						cube(size = [60, 60, 2600]);
					}
					translate(v = [-700, -2680, 0]) {
						cube(size = [60, 60, 1200.0000000000]);
					}
				}
				difference() {
					union() {
						translate(v = [-2420, -151.1111111111, 0]) {
							cube(size = [20, 40, 2960.0000000000]);
						}
						translate(v = [-2420, -242.2222222222, 0]) {
							cube(size = [20, 40, 2920.0000000000]);
						}
						translate(v = [-2420, -333.3333333333, 0]) {
							cube(size = [20, 40, 2880.0000000000]);
						}
						translate(v = [-2420, -424.4444444444, 0]) {
							cube(size = [20, 40, 2840.0000000000]);
						}
						translate(v = [-2420, -515.5555555556, 0]) {
							cube(size = [20, 40, 2800.0000000000]);
						}
						translate(v = [-2420, -606.6666666667, 0]) {
							cube(size = [20, 40, 2760.0000000000]);
						}
						translate(v = [-2420, -697.7777777778, 0]) {
							cube(size = [20, 40, 2720.0000000000]);
						}
						translate(v = [-2420, -788.8888888889, 0]) {
							cube(size = [20, 40, 2680.0000000000]);
						}
						translate(v = [-2420, -880.0000000000, 0]) {
							cube(size = [20, 40, 2640.0000000000]);
						}
						translate(v = [-2281.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-2182.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-2083.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1985.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1886.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1787.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1688.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1590.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1491.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1392.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1293.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1195.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-1096.2500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-997.5000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-898.7500000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
						translate(v = [-800.0000000000, -1020, 0]) {
							cube(size = [40, 20, 2600]);
						}
					}
					translate(v = [-1640, -1030.0000000000, 0]) {
						cube(size = [700, 40, 1500]);
					}
					translate(v = [-2430.0000000000, -800, 900]) {
						cube(size = [40, 600, 600]);
					}
					translate(v = [-2140, -1030.0000000000, 2150.0000000000]) {
						cube(size = [700, 40, 200]);
					}
				}
				union() {
					translate(v = [-680.0000000000, -2620, 930.0000000000]) {
						rotate(a = [41.2568806153, 0, 0]) {
							cube(size = [20, 2214.2516101886, 40]);
						}
					}
					translate(v = [0, 0, -132.9784944561]) {
						translate(v = [-680.0000000000, -2620, 930.0000000000]) {
							rotate(a = [41.2568806153, 0, 0]) {
								cube(size = [20, 2214.2516101886, 40]);
							}
						}
					}
					translate(v = [0, 0, -265.9569889122]) {
						translate(v = [-680.0000000000, -2620, 930.0000000000]) {
							rotate(a = [41.2568806153, 0, 0]) {
								cube(size = [20, 2214.2516101886, 40]);
							}
						}
					}
					translate(v = [0, 0, -398.9354833683]) {
						translate(v = [-680.0000000000, -2620, 930.0000000000]) {
							rotate(a = [41.2568806153, 0, 0]) {
								cube(size = [20, 2214.2516101886, 40]);
							}
						}
					}
				}
			}
		}
	}
}
/***********************************************
*********      SolidPython code:      **********
************************************************
 
from functools import *
from solid import *
from solid.utils import *

# http://www.openscad.org/cheatsheet/index.html

hauteur_murs = 3000

# Description chambre
def chambre(hauteur_murs = hauteur_murs,
    largeur_chambre = 2500,
    longueur_chambre = 4000,
    epaisseur_mur = 50,
    epaisseur_sol = 50,
    largeur_porte = 1000,
    # Description des couleurs
    couleur_plafond = color([0.9, 0.9, 0.9, 0.2]),
    couleur_mur = color([0.9, 0.9, 0.9, 0.5]),
    couleur_sol = color([0.5, 0.4, 0.3, 0.5])):
    # Definition des murs
    mur_ne = translate([-largeur_chambre,0,0])(cube([largeur_chambre, epaisseur_mur, hauteur_murs]))
    mur_so = translate([0,-longueur_chambre,0])(cube([epaisseur_mur, longueur_chambre, hauteur_murs]))
    sol = translate([-largeur_chambre-largeur_porte,-longueur_chambre-largeur_porte,-epaisseur_sol])(cube([largeur_chambre+largeur_porte+epaisseur_mur, longueur_chambre+largeur_porte+epaisseur_mur, epaisseur_sol]))
    plafond = translate([0,0,epaisseur_sol+hauteur_murs])(sol)
    return couleur_mur(mur_ne + mur_so) + couleur_sol(sol)# + couleur_plafond(plafond)

# Description des couleurs
couleur_lit = color([0.8, 0.7, 0.6, 1])
blanc = color([1, 1, 1, 1])
rouge = color([0.8, 0.1, 0.1, 1])
bleu = color([0.1, 0.3, 0.9, 1])
jaune = color([0.9, 0.8, 0.3, 1])

# Dimensions lit


# Description lit
def lit(hauteur_etage = 1700,
    longueur_etage = 2400,
    largeur_etage = 1000,
    hauteur_barriere = 900,
    hauteur_lit = 150,
    epaisseur_lit = 30,
    hauteur_murs = hauteur_murs,
    nombre_marche = 6,
    largeur_marche = 700,
    profondeur_marche = 300,
    epaisseur_planche_marche = 20,
    section_pillier = 60,
    largeur_montant = 40,
    epaisseur_montant = 20,
    largeur_espace_cible = 60,
    hauteur_porte = 1500):
    hauteur_marche = (hauteur_etage-epaisseur_planche_marche*(nombre_marche+1))/nombre_marche
    # Le sommier
    lit_etage = translate([-longueur_etage, -largeur_etage, hauteur_etage-epaisseur_lit])(
        cube([longueur_etage, largeur_etage, epaisseur_lit]))
    lit_bas = translate([-longueur_etage, -largeur_etage, hauteur_lit-epaisseur_lit])(
        cube([longueur_etage, largeur_etage, epaisseur_lit]))
    def pilliers():
        pillier_1 = translate([-longueur_etage, -largeur_etage, 0])(
            cube([section_pillier, section_pillier, hauteur_barriere + hauteur_etage]))
        pillier_2 = translate([-longueur_etage, -section_pillier, 0])(
            cube([section_pillier, section_pillier, hauteur_murs]))
        pillier_3 = translate([-largeur_marche, -largeur_etage, 0])(
            cube([section_pillier, section_pillier, hauteur_barriere + hauteur_etage]))
        pillier_4 = translate([-largeur_marche, -(section_pillier+largeur_etage+epaisseur_planche_marche+(nombre_marche-1)*(profondeur_marche+epaisseur_planche_marche)), 0])(
            cube([section_pillier, section_pillier, hauteur_marche+2*epaisseur_planche_marche+hauteur_barriere]))
        return pillier_1 + pillier_2 + pillier_3 + pillier_4
    # La barriere
    def barriere():
        result = []
        # Cote
        largeur_cote = largeur_etage - 2*section_pillier
        hauteur_cote_haut = hauteur_murs
        hauteur_cote_bas = hauteur_barriere + hauteur_etage
        nombre_montant_cote = int(ceil((largeur_cote-largeur_espace_cible)/float(largeur_espace_cible+largeur_montant)))
        largeur_espace_cote = (largeur_cote-largeur_espace_cible)/float(nombre_montant_cote)-largeur_montant
        for i in range(nombre_montant_cote):
            result.append(translate([-(longueur_etage+epaisseur_montant), -(section_pillier+largeur_montant+largeur_espace_cote+i*(largeur_montant+largeur_espace_cote)), 0])(
                cube([epaisseur_montant, largeur_montant, hauteur_cote_haut + (1+i)*(hauteur_cote_bas-hauteur_cote_haut)/(1+nombre_montant_cote)])))
        # Face
        largeur_face = longueur_etage - section_pillier - largeur_marche
        hauteur_face = hauteur_barriere + hauteur_etage
        nombre_montant_face = int(ceil((largeur_face-largeur_espace_cible)/float(largeur_espace_cible+largeur_montant)))
        largeur_espace_face = (largeur_face-largeur_espace_cible)/float(nombre_montant_face)-largeur_montant
        for i in range(nombre_montant_face):
            result.append(translate([-longueur_etage+section_pillier+largeur_espace_face+i*(largeur_montant+largeur_espace_face), -largeur_etage-epaisseur_montant, 0])(
                cube([largeur_montant, epaisseur_montant, hauteur_face])))
        porte = translate([-longueur_etage+section_pillier+700, -largeur_etage-1.5*epaisseur_montant, 0])(
            cube([700, 2*epaisseur_montant, hauteur_porte]))
        fenetre_1 = translate([-longueur_etage-1.5*epaisseur_montant, -800, hauteur_porte-600])(
            cube([2*epaisseur_montant, 600, 600]))
        fenetre_2 = translate([-longueur_etage+section_pillier+200, -largeur_etage-1.5*epaisseur_montant, hauteur_etage+0.5*hauteur_barriere])(
            cube([700, 2*epaisseur_montant, 200]))
        return reduce(lambda r,x: r+x, result[1:], result[0])-porte-fenetre_1-fenetre_2
    # Rambarde
    def rambarde():
        result = []
        longueur = epaisseur_planche_marche+(nombre_marche-1)*(profondeur_marche+epaisseur_planche_marche)
        angle = atan((hauteur_etage-hauteur_marche-epaisseur_planche_marche)/float(longueur))
        angle_deg = 180*angle/3.14
        principale = translate([-largeur_marche+0.5*(section_pillier-epaisseur_montant), -largeur_etage-longueur, 0.7*hauteur_barriere + 2*epaisseur_planche_marche + hauteur_marche])(
            rotate([angle_deg,0,0])(cube([epaisseur_montant, longueur/cos(angle)+section_pillier, largeur_montant])))
        result.append(principale)
        result.append(translate([0,0,-4*(largeur_montant+largeur_espace_cible)/cos(angle)])(principale))
        # Barres verticales
        hauteur_haut = hauteur_etage + hauteur_barriere
        hauteur_bas = hauteur_barriere + 2*epaisseur_planche_marche + hauteur_marche
        nombre_montant = int(ceil((longueur-largeur_espace_cible)/float(largeur_espace_cible+largeur_montant)))
        largeur_espace = (longueur-largeur_espace_cible)/float(nombre_montant)-largeur_montant
        for i in range(nombre_montant):
            result.append(translate([-largeur_marche,
            -largeur_etage - largeur_espace_cible - section_pillier - i*(largeur_montant+largeur_espace_cible),
            hauteur_etage + (1+i)*(hauteur_bas-hauteur_haut)/(nombre_montant)])(
                cube([epaisseur_montant, largeur_montant, 0.9*hauteur_barriere])))
        return reduce(lambda r,x: r+x, result[1:], result[0])
    def rambarde_h():
        result = []
        longueur = epaisseur_planche_marche+(nombre_marche-1)*(profondeur_marche+epaisseur_planche_marche)
        angle = atan((hauteur_etage-hauteur_marche-epaisseur_planche_marche)/float(longueur))
        angle_deg = 180*angle/3.14
        principale = translate([-largeur_marche+0.5*(section_pillier-epaisseur_montant), -largeur_etage-longueur, 0.7*hauteur_barriere + 2*epaisseur_planche_marche + hauteur_marche])(
            rotate([angle_deg,0,0])(cube([epaisseur_montant, longueur/cos(angle)+section_pillier, largeur_montant])))
        result.append(principale)
        result.append(translate([0,0,-1*(largeur_montant+largeur_espace_cible)/cos(angle)])(principale))
        result.append(translate([0,0,-2*(largeur_montant+largeur_espace_cible)/cos(angle)])(principale))
        result.append(translate([0,0,-3*(largeur_montant+largeur_espace_cible)/cos(angle)])(principale))
        return reduce(lambda r,x: r+x, result[1:], result[0])

    # Description escalier
    def escalier(epaisseur_porte = 10,
        # Description des couleurs
        couleur_lit = couleur_lit,
        couleur_porte_1 = blanc,
        couleur_porte_2 = rouge,
        couleur_porte_3 = jaune):
        hauteur_marche = (hauteur_etage-epaisseur_planche_marche*(nombre_marche+1))/nombre_marche
        # Paroie verticale
        def v(i,j):
            return translate([-largeur_marche,
                -(largeur_etage+epaisseur_planche_marche)-i*(profondeur_marche+epaisseur_planche_marche),
                j*(hauteur_marche+epaisseur_planche_marche)])(
                cube([largeur_marche, epaisseur_planche_marche, 2*epaisseur_planche_marche+hauteur_marche]))
        # Paroie horizontale
        def h(i,j):
            return translate([-largeur_marche,
                -(largeur_etage+profondeur_marche+2*epaisseur_planche_marche)-i*(profondeur_marche+epaisseur_planche_marche),
                j*(hauteur_marche+epaisseur_planche_marche)])(
                cube([largeur_marche, 2*epaisseur_planche_marche+profondeur_marche, epaisseur_planche_marche]))
        # Porte
        def p(i,j,l,h):
            return translate([-(largeur_marche+epaisseur_porte),
                -(largeur_etage+profondeur_marche+epaisseur_planche_marche)-(i+l-1)*(profondeur_marche+epaisseur_planche_marche),
                epaisseur_planche_marche+j*(hauteur_marche+epaisseur_planche_marche)])(
                cube([epaisseur_porte, profondeur_marche+(l-1)*(profondeur_marche+epaisseur_planche_marche), hauteur_marche+(h-1)*(hauteur_marche+epaisseur_planche_marche)]))
        H =[[1,0,0,0,0,0],
            [1,1,0,0,0,0],
            [0,0,1,0,0,0],
            [0,0,1,1,0,0],
            [1,1,0,0,1,0],
            [1,1,1,1,1,0]]
        V =[[1,0,0,0,0,0],
            [1,1,0,0,0,0],
            [1,0,1,0,0,0],
            [1,0,1,1,0,0],
            [1,0,1,0,1,0],
            [1,0,1,0,1,1]]
        result = []
        for i in range(nombre_marche):
            for j in range(nombre_marche):
                if H[j][i]==1:
                    result.append(h(i,nombre_marche-j-1))
                if V[j][i]==1:
                    result.append(v(i,nombre_marche-j-1))
        return couleur_lit(reduce(lambda r,x: r+x, result[1:], result[0]))\
        + couleur_porte_1(p(0,1,2,3))\
        + couleur_porte_2(p(2,0,2,2))\
        + couleur_porte_3(p(0,4,1,1))
    # Construction
    return escalier() + couleur_lit(lit_etage+lit_bas+pilliers()+barriere()+rambarde_h())

tancrede = translate([-1000,-3000,0])(cylinder(h=1000,r=100))
lit_tancrede = chambre() + lit()
#lit_tancrede = lit()

if __name__ == '__main__':
    scad_render_to_file(lit_tancrede,
        'lit_tancrede.scad',
        file_header='$fn={fn};'.format(fn=100))
 
 
************************************************/
